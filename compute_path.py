from neomodel import db
from graph_model import Waypoint
from osm_service import OsmService


def compute_path(start_id,end_id):
    cypher_query = f"""MATCH (source:Waypoint {{osm_id: {start_id} }}), (target:Waypoint {{osm_id: {end_id} }})
                        CALL gds.shortestPath.astar.stream('waypointGraph', {{
                            sourceNode: source,
                            targetNode: target,
                            latitudeProperty: 'latitude',
                            longitudeProperty: 'longitude',
                            relationshipWeightProperty: 'length'
                        }})
                        YIELD index, sourceNode, targetNode, totalCost, nodeIds, costs, path
                        RETURN
                            index,
                            gds.util.asNode(sourceNode).osm_id AS sourceNodeName,
                            gds.util.asNode(targetNode).osm_id AS targetNodeName,
                            totalCost,
                            costs,
                            [nodeId IN nodeIds | gds.util.asNode(nodeId).osm_id] AS path_ids,
                            nodes(path) as path
                        ORDER BY index"""
    result = db.cypher_query(cypher_query)
    path = result[0][0][5]
    osm = OsmService()
    points = []
    for node in path:
        point = osm.get_point(node)
        points.append(point)
        print(point)
    print("to display in leaflet add this code:")
    for point in points:
        print(f"map.addLayer(L.marker(L.latLng({point.latitude},{point.longitude})))")

compute_path(136830532,448882027)