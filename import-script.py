import pickle
from collections import defaultdict
from pathlib import Path
from neomodel import db
from tqdm import tqdm

from graph_model import Waypoint
from osm_service import OsmService
def getgraph():
    if Path("graph.p").exists():
        return pickle.load(open("graph.p","rb"))
    osm = OsmService()

    graph = defaultdict(lambda :list())

    airports = osm.get_points(icao=OsmService.NOT_NULL)

    stack = list(map(lambda x:x[0],airports))
    progress = tqdm(desc="Graphing")
    while stack:
        node = stack.pop()
        buckets = osm.get_azimuth_buckets_of_point(node)
        edges = graph[node]
        for b in buckets:
            id = b[0]
            dist = b[4]
            if id not in graph:
                stack.append(id)
            edges.append((id,dist))
        progress.total = len(stack)+len(graph)
        progress.update(1)
    graph = dict(graph)
    pickle.dump(graph,open("graph.p","wb"))
    return graph
graph = getgraph()

waypoints = dict()
for node in graph:
    waypoint = Waypoint.nodes.get_or_none(osm_id=node)
    if not waypoint:
        waypoint = Waypoint(osm_id=node)
        waypoint.save()
    waypoints[node] = waypoint
for node in tqdm(graph):
    osm = OsmService()
    point = osm.get_point(osm_id=node)
    waypoint = Waypoint.nodes.get_or_none(osm_id=node)
    waypoint.latitude = point.latitude
    waypoint.longitude = point.longitude
    waypoint.save()
progress = tqdm(desc="edge",total=sum(len(edges) for edges in graph.values()))
for node in graph:
    for edge in graph[node]:
        target,length = edge
        waypoints[node].neighbours.connect(waypoints[target],{"length":length})
        progress.update()
