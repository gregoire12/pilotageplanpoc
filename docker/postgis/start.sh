
# must be run from postgis dir
docker pull postgis/postgis
docker-compose up -d
docker build --t osm2pgsql-importer:latest .
echo "RUN in the container : 'osm2pgsql --create --slim --cache 2000 --database planner --username postgis --password --host localhost --port 55432 --style /osm/style.style /osm/ile-de-france-latest.osm.pbf'"
docker run -i -t --rm --net=host -v $(pwd)/:/osm osm2pgsql-importer
