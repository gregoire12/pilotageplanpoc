"""This file is a poc for a neo4j import osm script from a postgis database in order to build a routable graph"""
from neomodel import config, StructuredNode, IntegerProperty, StringProperty, RelationshipTo, StructuredRel, \
    FloatProperty

config.DATABASE_URL = 'bolt://neo4j:plannerpoc@localhost:7687'  # default


class NeighbourRel(StructuredRel):
    length = FloatProperty()


class Waypoint(StructuredNode):
    osm_id = IntegerProperty()
    neighbours = RelationshipTo("Waypoint", "NEIGHBOUR", model=NeighbourRel)
    latitude = FloatProperty()
    longitude = FloatProperty()