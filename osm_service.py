import psycopg2


class Point:

    def __init__(self,osm_id,longitude,latitude,name) -> None:
        super().__init__()
        self.osm_id = osm_id
        self.latitude=latitude
        self.longitude=longitude
        self.name=name

    def __str__(self) -> str:
        return f"{self.osm_id} - \t{self.latitude},{self.longitude}\t {self.name}"


class OsmService:
    NOT_NULL = "NOT NULL"

    def __init__(self) -> None:
        self.conn = psycopg2.connect(dbname="planner", user="postgis", password="postgis", host="localhost",
                                     port="55432")
        self.create_temp_tables()
    def create_temp_tables(self):
        with self.conn.cursor() as cursor:
            query = """CREATE TEMP TABLE temp_visible_waypoints AS
                        (SELECT * FROM planet_osm_point as way
                        WHERE  way.leisure = 'stadium' OR way.leisure = 'park' OR way.leisure = 'golf_course' OR way.aeroway = 'aerodrome' OR way.aeroway = 'airport' OR way.man_made = 'tower' OR way.bridge = 'yes' OR way.bridge = 'suspension' OR way.bridge = 'viaduct' OR way.natural = 'wood' OR way.natural = 'water' )"""
            cursor.execute(query)
        with self.conn.cursor() as cursor:
            query = """CREATE TEMP TABLE temp_visible_wayareas AS
                        (SELECT * FROM planet_osm_polygon as way
                        WHERE  way.leisure = 'stadium' OR way.leisure = 'park' OR way.leisure = 'golf_course' OR way.aeroway = 'aerodrome' OR way.aeroway = 'airport' OR way.man_made = 'tower' OR way.bridge = 'yes' OR way.bridge = 'suspension' OR way.bridge = 'viaduct' OR way.natural = 'wood' OR way.natural = 'water' )"""
            cursor.execute(query)

    def get_point(self, osm_id):
        query = """SELECT point.osm_id,
                            ST_X(ST_Transform(point.way,4326)) AS lon,
                            ST_Y(ST_Transform(point.way,4326)) AS lat,
                            point.name FROM planet_osm_point as point where point.osm_id=%s"""
        with self.conn.cursor() as cursor:
            cursor.execute(query,(osm_id,))
            result = cursor.fetchone()
        return Point(*result)
    def get_way(self,osm_id):
        query="""SELECT way.osm_id"""
    def get_points(self, *args, **kwargs):
        """
        returns an iterator of Points( tbd)
        columns values are specified with named arguments
        to specify Not null use the OsmService.NotNull value
        :return:
        """
        with self.conn.cursor() as cursor:
            query = "SELECT * FROM planet_osm_point as points where "
            return self.fetch_all(cursor, kwargs, query)

    def get_ways(self, *args, **kwargs):
        with self.conn.cursor() as cursor:
            query = "SELECT * FROM planet_osm_polygon as ways where "
            return self.fetch_all(cursor, kwargs, query)
    def get_azimuth_buckets_of_point(self,point_id):
        with self.conn.cursor() as cursor:
            query = """WITH azimuth_buckets AS (
                          SELECT way.osm_id,
                                 way.name,
                                 ST_X(points.way) AS lon,
                                 ST_Y(points.way) AS lat,
                                 ST_Distance(way.way, points.way) AS distance,
                                 degrees(ST_Azimuth(ST_Centroid(way.way), points.way)) AS azimuth,
                                 floor(degrees(ST_Azimuth(ST_Centroid(way.way), points.way)) / 5) * 5 AS azimuth_bucket_floor
                          FROM planet_osm_point AS points,
                               temp_visible_waypoints AS way
                          where points.osm_id =%s
                        ) select osm_id,name,lon,lat,distance,azimuth,
                                 azimuth_bucket_floor || '-' || (azimuth_bucket_floor + 5) AS azimuth_bucket
                        FROM (
                          SELECT *,
                                 RANK() OVER (PARTITION BY azimuth_bucket_floor ORDER BY distance ASC) AS r
                          FROM azimuth_buckets
                        ) AS ranked_buckets
                        WHERE r = 1;
                        """
            cursor.execute(query,(point_id,))
            return cursor.fetchall()
    def get_azymuth_buckets_of_all_waypoints(self,):
        with self.conn.cursor() as cursor:
            query = """WITH azimuth_buckets AS (
                                      SELECT way.osm_id,
                                             way.name,
                                             ST_X(points.way) AS lon,
                                             ST_Y(points.way) AS lat,
                                             ST_Distance(way.way, points.way) AS distance,
                                             degrees(ST_Azimuth(ST_Centroid(way.way), points.way)) AS azimuth,
                                             floor(degrees(ST_Azimuth(ST_Centroid(way.way), points.way)) / 5) * 5 AS azimuth_bucket_floor
                                      FROM planet_osm_point AS points,
                                           planet_osm_point AS way
                                      where points.osm_id =%s AND """ + \
                    self.get_waypoint_selector(prefix="way") + """ 
                                    ) select osm_id,name,lon,lat,distance,azimuth,
                                             azimuth_bucket_floor || '-' || (azimuth_bucket_floor + 5) AS azimuth_bucket
                                    FROM (
                                      SELECT *,
                                             RANK() OVER (PARTITION BY azimuth_bucket_floor ORDER BY distance ASC) AS r
                                      FROM azimuth_buckets
                                    ) AS ranked_buckets
                                    WHERE r = 1;
                                    """
    def get_waypoint_selector(self,prefix):
        attributes = [
            (prefix+'.leisure',"'stadium'"),
            (prefix+'.leisure',"'park'"),
            (prefix+'.leisure',"'golf_course'"),
            (prefix+'.aeroway',"'aerodrome'"),
            (prefix+'.aeroway',"'airport'"),
            (prefix+'.man_made',"'tower'"),
            (prefix+'.bridge',"'yes'"),
            (prefix+'.bridge',"'suspension'"),
            (prefix+'.bridge',"'viaduct'"),
            (prefix+'.natural',"'wood'"),
            (prefix+'.natural',"'water'"),
        ]
        return "( "+ " OR ".join([' = '.join(attribute) for attribute in attributes]) +" )"
    def fetch_all(self, cursor, kwargs, query):
        query += self.generate_conditions(**kwargs)
        arguments = self.generate_arguments(kwargs)
        cursor.execute(query, arguments)
        results = cursor.fetchall()
        return results

    def generate_arguments(self, kwargs):
        arguments = tuple(filter(lambda x: x != OsmService.NOT_NULL, kwargs.values()))
        return arguments

    def generate_conditions(self, **kwargs) -> str:
        conditions = []
        for key in kwargs:
            if (kwargs[key] == OsmService.NOT_NULL):
                conditions.append(key + " is NOT NULL")
            else:
                conditions.append(key + " = (%s)")
        return " AND ".join(conditions)


